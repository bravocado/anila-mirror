# Changelog

### v0.0.1

- first structuring
- copy and modifying grid inuit.css


### v0.0.2

- change directory custom vendor folder
- creating a specified media condition for custom CSS
  you may want to use @import or another includes for your custom CSS
- added type.scss to vendor
- renaming style-mobile.css to style-phone.css


### v0.0.3

- adding meta tag to template
- adding "blocked" class for no margin and no padding grid system
- adding vars.scss for custom variables
- adding beautons from inuit.css


### v0.0.4

- added flat, rounded, gradient buttons
- dropping ms prefix from vendor mixin'


### v0.0.4-a

- added grid centered
- responsive grid system version-a
- button version-a
- fix conflict

### v0.0.5

- adding box sizing to reset
- move hill to general
- modify button variables and functions