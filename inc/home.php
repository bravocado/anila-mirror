<div class="container">

	<div class="grid blocked">
		<h4>GRID SYSTEM</h4>

		<h5>Full Grid</h5>

	   <div class="grid__item  one-third">
	       <div class="block_test blue">
	       		1/3
	       </div>
	   </div><!--
	--><div class="grid__item  two-thirds">
	       <div class="block_test blue">
	       		2/3
	       </div>
	   </div><!--

	--><div class="grid__item  one-half">
	       <div class="block_test blue">
	       		1/2
	       </div>
	   </div><!--
	--><div class="grid__item unblock one-quarter">
	       <div class="block_test blue">
	       		1/4
	       </div>
	   </div><!--
	--><div class="grid__item unblock one-quarter">
	       <div class="block_test blue">
	       		1/4
	       </div>
	   </div>
	</div> <!-- end grid -->

	<div class="grid">
		<div class="grid__item one-tenth">
			<div class="block_test blue">
				1/10
			</div>
		</div><!--
		--><div class="grid__item two-tenths">
			<div class="block_test blue">
				2/10
			</div>
		</div><!--
		--><div class="grid__item four-tenths">
			<div class="block_test blue">
				4/10
			</div>
		</div><!--
		--><div class="grid__item three-tenths">
			<div class="block_test blue">
				3/10
			</div>
		</div>
	</div><!-- end grid -->

	<div class="grid">
		<div class="grid__item one-twelfth">
			<div class="block_test blue">
				1/12
			</div>
		</div><!--
		--><div class="grid__item one-twelfth">
			<div class="block_test blue">
				1/12
			</div>
		</div><!--
		--><div class="grid__item one-twelfth">
			<div class="block_test blue">
				1/12
			</div>
		</div><!--
		--><div class="grid__item one-twelfth">
			<div class="block_test blue">
				1/12
			</div>
		</div><!--
		--><div class="grid__item one-twelfth">
			<div class="block_test blue">
				1/12
			</div>
		</div><!--
		--><div class="grid__item one-twelfth">
			<div class="block_test blue">
				1/12
			</div>
		</div><!--
		--><div class="grid__item one-twelfth">
			<div class="block_test blue">
				1/12
			</div>
		</div><!--
		--><div class="grid__item one-twelfth">
			<div class="block_test blue">
				1/12
			</div>
		</div><!--
		--><div class="grid__item one-twelfth">
			<div class="block_test blue">
				1/12
			</div>
		</div><!--
		--><div class="grid__item one-twelfth">
			<div class="block_test blue">
				1/12
			</div>
		</div><!--
		--><div class="grid__item one-twelfth">
			<div class="block_test blue">
				1/12
			</div>
		</div><!--
		--><div class="grid__item one-twelfth">
			<div class="block_test blue">
				1/12
			</div>
		</div>
	</div> <!-- end grid -->
	
	<br><br><br>
	<h5 class="txt-c">centered Grid</h5>
	<br>
	<div class="grid">
		<div class="grid__item centered eleven-twelfths">
			<div class="block_test blue">
				11/12
			</div>
		</div><!--
		--><div class="grid__item centered ten-twelfths">
			<div class="block_test blue">
				10/12
			</div>
		</div><!--
		--><div class="grid__item centered nine-twelfths">
			<div class="block_test blue">
				9/12
			</div>
		</div><!--
		--><div class="grid__item centered eight-twelfths">
			<div class="block_test blue">
				8/12
			</div>
		</div><!--
		--><div class="grid__item centered seven-twelfths">
			<div class="block_test blue">
				7/12
			</div>
		</div><!--
		--><div class="grid__item centered six-twelfths">
			<div class="block_test blue">
				6/12
			</div>
		</div><!--
		--><div class="grid__item centered five-twelfths">
			<div class="block_test blue">
				5/12
			</div>
		</div><!--
		--><div class="grid__item centered four-twelfths">
			<div class="block_test blue">
				4/12
			</div>
		</div><!--
		--><div class="grid__item centered three-twelfths">
			<div class="block_test blue">
				3/12
			</div>
		</div><!--
		--><div class="grid__item centered two-twelfths">
			<div class="block_test blue">
				2/12
			</div>
		</div><!--
		--><div class="grid__item centered one-twelfth">
			<div class="block_test blue">
				1/12
			</div>
		</div>
	</div><!-- end grid -->

	<br><br>
	<p class="txt-c"><small>Tablet and Phone grid -- it is what it is</small></p>
	<div class="grid">
	   <div class="grid__item  one-third">
	       <div class="block_test blue">
	       		1/3
	       </div>
	   </div><!--
	--><div class="grid__item  two-thirds">
	       <div class="block_test blue">
	       		2/3
	       </div>
	   </div><!--

	--><div class="grid__item  one-half">
	       <div class="block_test blue">
	       		1/2
	       </div>
	   </div><!--
	--><div class="grid__item unblock one-quarter">
	       <div class="block_test blue">
	       		1/4
	       </div>
	   </div><!--
	--><div class="grid__item unblock one-quarter">
	       <div class="block_test blue">
	       		1/4
	       </div>
	   </div>
	</div>

	<br><br>
	<h5>Pull and push? use float on our helper. i won't make my css more heavy.</h5>
	<div class="grid cf">
	   <div class="grid__item  one-quarter flo-l">
	       <div class="block_test blue">
	       		1/4
	       </div>
	   </div><!--
	--><div class="grid__item  one-quarter flo-r">
	       <div class="block_test blue">
	       		1/4
	       </div>
	   </div>
	</div>

	<br><br><br>

	<div class="grid">
		<div class="grid__item one-first">
			<h1>THIS IS A HEADLINE ONE</h1>
			<h2>THIS IS HEADLINE TWO</h2>
			<h3>THIS IS HEADLINE THREE</h3>
			<h4>THIS IS HEADLINE FOUR</h4>
			<h5>THIS IS HEADLINE FIVE</h5>
			<h6>THIS IS HEADLINE SIX</h6>
		</div>

		<div class="grid__item one-first">
			<p><b>Paragraph</b> -- <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, modi, in quam aspernatur quo esse mollitia provident deleniti soluta neque tempore quidem rerum non ipsum itaque ullam eius! Fugit, iusto.</span>
			<span>Dolorem, labore, blanditiis et harum eos voluptatum id recusandae illo dignissimos omnis eum dicta saepe fugit eveniet tempora iste fuga odio consequatur ab repellat excepturi reiciendis nisi consectetur at non.</span>
			<span>Maiores, dignissimos, totam cum facilis dolorum excepturi beatae nulla repellat voluptas pariatur quidem molestias eligendi culpa magni ut placeat fugiat temporibus a officia cumque harum odit omnis ex fugit earum.</span></p>
		</div>

	</div>

	<br><br><br>

	<div class="grid blocked">
		<div class="grid__item one-first">
			<div>
				<h5>HARD FLAT BUTTONS</h5>
				<span class="btn btn-primary flat btn-hard">Button Primary</span>
				<span class="btn btn-whatever flat btn-hard">Button Whatever</span>
				<span class="btn disabled btn-hard">Button Deactived</span>
			</div>

			<br><br>

			<div>
				<h5>SOFT FLAT BUTTONS</h5>
				<span class="btn btn-primary flat btn-soft">Button Primary</span>
				<span class="btn btn-whatever flat btn-soft">Button Whatever</span>
				<span class="btn disabled btn-soft">Button Deactived</span>
			</div>
			
			<br><br>

			<div>
				<h5>HARD GRADIENT BUTTONS</h5>
				<span class="btn btn-primary pretty btn-hard">Button Primary</span>
				<span class="btn btn-whatever pretty btn-hard">Button Whatever</span>
				<span class="btn disabled pretty btn-hard">Button Deactived</span>
			</div>

			<br><br>

			<div>
				<h5>SOFT GRADIENT BUTTONS</h5>
				<span class="btn btn-primary pretty btn-soft">Button Primary</span>
				<span class="btn btn-whatever pretty btn-soft">Button Whatever</span>
				<span class="btn disabled pretty btn-soft">Button Deactived</span>
			</div>

			<br><br>

			<div>
				<h5>BUTTON SIZES</h5>
				<span class="btn btn-primary flat btn-hard btn-xlarge">Button xLarge</span>
				<span class="btn btn-whatever flat btn-hard btn-large">Button Large</span>
				<span class="btn btn-primary flat btn-hard">Button Normal</span>
				<span class="btn btn-whatever flat btn-hard btn-mini">Button Mini</span>
			</div>

			<br><br>

			<div>
				<h5>BLOCK BUTTONS</h5>
				<span class="btn btn-whatever flat btn-hard btn-block on-all">Block on All</span>
				<br><br>
				<span class="btn btn-primary flat btn-hard btn-block on-desk">Block on Desk</span>
				<br><br>
				<span class="btn btn-whatever flat btn-hard btn-block on-tb">Block on Tablet</span>
				<br><br>
				<span class="btn btn-primary flat btn-hard btn-block on-ph">Block on Phone</span>
			</div>

		</div>
	</div>

</div><!-- end container -->
